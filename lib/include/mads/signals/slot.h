// ReSharper disable CppDoxygenUnresolvedReference


/**
* @file slot.h
* @brief Provides functionality for managing and invoking slots in a signal-slot system.
*
* This header defines the data structures and functions for creating, managing, and freeing
* slots. A slot is a function that can be invoked in response to a signal, with optional
* context and data passed as arguments.
*/


#ifndef MADS_SIGNALS_SLOT_H
#define MADS_SIGNALS_SLOT_H

#ifdef __cplusplus
extern "C" {
#endif

#include <mads_export.h>

/**
 * @brief Function pointer type for slot functions.
 * @param[in] GenericPointer A pointer to a context that triggers the signal
 * @param[in] GenericPointer A pointer to data send by the signal
 */
typedef void (*mads_slot_fn)(void *, void *);


/**
 * @brief Data structure that represents a mads slot.
 */
typedef struct
{
    int blocked; ///< @brief Whether the slot is blocked or not
    void *context; ///< @brief A pointer to the context that triggers the signal
    mads_slot_fn function; ///<@brief Function pointer to the slot function
} mads_slot_t;


/**
 * @brief Function to create a new slot.
 * @param[in] context Context associated with the slot
 * @param[in] function Slot function
 * @return Pointer to a created slot
 */
MADS_EXPORT mads_slot_t *mads_slot_create(void *context, mads_slot_fn function);


/**
 * @brief  Function to free a slot and all allocated memory.
 * @param[in,out] slot The slot to free
 */
MADS_EXPORT void mads_slot_free(mads_slot_t **slot);

#ifdef __cplusplus
}
#endif

#endif //MADS_SIGNALS_SLOT_H
